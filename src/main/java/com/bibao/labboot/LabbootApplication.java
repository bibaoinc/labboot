package com.bibao.labboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabbootApplication.class, args);
	}
}
